const nGst = 18; // total percentage of GST
const nPhonePrice = 20000; // Phone Price
const nAccessoryPrice = 1000; // Accessory Price
const nThreshold = 40000; // Budget Price

var nBalance = 70000; // Account Balance 
var nShopping = 0; // Total amount Of Shopping had done

var nNoPhone = 0; // No of Phone buy
var nNoAccessory = 0; // No of Accessory buy


// Display in List
document.getElementById('nGst').innerHTML = nGst+"%"
document.getElementById('nThreshold').innerHTML = "$"+nThreshold.toFixed(2)
document.getElementById('nBalance').innerHTML = "$"+nBalance.toFixed(2)
document.getElementById('nPhonePrice').innerHTML = "$"+nPhonePrice.toFixed(2)
document.getElementById('nAccessoryPrice').innerHTML = "$"+nAccessoryPrice.toFixed(2)

// Display in Form
document.getElementById('nBillPhonePrice').innerHTML = "$"+nPhonePrice.toFixed(2)
document.getElementById('nBillAccessoryPrice').innerHTML = "$"+nAccessoryPrice.toFixed(2)

//  Gst Counter For Both Mobile and Accessory 
function getFinalPriceWithGst(nGstTotal) {
    return nGstTotal + (nGstTotal * nGst / 100)
}

// temp Bill variable for Count Shopping Amount
var nBill = nShopping + getFinalPriceWithGst(nPhonePrice)


while (nBill <= nThreshold) {
    // Mobile Price Add in shopping variable include gst charge
    nShopping += getFinalPriceWithGst(nPhonePrice)
    nNoPhone++

    // Add Accessory amount with shopping in bill variable 
    nBill = nShopping + getFinalPriceWithGst(nAccessoryPrice)

    if (nBill <= nThreshold) {

        // Add Accessory Price in shopping variable include gst charge
        nShopping += getFinalPriceWithGst(nAccessoryPrice)
        nNoAccessory++
    }

    // check next next increment of mobile price in temp variable bill
    nBill = nShopping + getFinalPriceWithGst(nPhonePrice)
}


// display number of Phone and Accessory buy
document.getElementById('nNoPhone').innerHTML = nNoPhone
document.getElementById('nNoAccessory').innerHTML = nNoAccessory

var nPhoneFinalPrice = nNoPhone * nPhonePrice
var nAccessoryFinalPrice = nNoAccessory * nAccessoryPrice


document.getElementById('nPhoneFinalPrice').innerHTML = "$"+nPhoneFinalPrice.toFixed(2)
document.getElementById('nAccessoryFinalPrice').innerHTML = "$"+nAccessoryFinalPrice.toFixed(2)

document.getElementById('nShopping').innerHTML = "$"+nShopping.toFixed(2)

/*

nBalance = nBalance - nShopping
console.log(" ")
console.log("Total Number of Phones buy : " + nNoPhone)
console.log("Total Number of Accessory buy : " + nNoAccessory)
console.log("Total Shopping : $" + nShopping)
console.log("Latest Balance : $" + nBalance)

*/